# Clear Linux image builder

This is a **completely unofficial hobby project** to try and build CLR cloud
images via CI. It uses a 2-stage approach where we first build an OCI image with
the CLR installer, then turn around and use that to actually create a disk image
that can be uploaded to a cloud host.

## Disclaimer

Again - this is a **completely unofficial hobby project** that is not in any way
associated with Intel other than that it uses Clear Linux. And frankly, it's
just a little thing I'm doing in my spare time; you really shouldn't rely on it
in any way and if you want this functionality you should probably go implement
it yourself because that's the kind of thing you should control in-house. Just
my two cents.

## TODOs

* Actually finish implementing and testing this thing and make sure it even works
* Support both UEFI and BIOS, either as 2 images or a single combined image
* Support+test a bunch of different cloud hosts
* Finish writing this README
* Bump to latest version of kaniko

## Use

TODO

Grab the out.img artifact from the latest pipeline and upload it to the cloud
provider, then create a VM from it.

## License

This repo is MIT (see LICENSE file), but note that that only covers the contents
of this repo (builds scripts, basically), not the contents of the generated
images, not the clr-installer program, nothing else, just the stuff I wrote in
this git repo!

